import express from 'express';

// define the server port
const PORT: number = 8080;

// create the server
const app: express.Application = express();

// configure server
app.use(express.json());
app.use(express.urlencoded());

// define routes
app.get('/', (request: express.Request, response: express.Response) => {
	response.send('Hello!');
});

app.post('/greet', (request: express.Request, response: express.Response) => {
	response.send(`Hello, ${request.body.name}!`);
});

// start the server on the specified port
app.listen(PORT, () => {
	console.log(`Server started at http://localhost:${PORT}!`);
});
