import rp from "request-promise";

// define the API endpoint for the requests
const SUM_URL: string = "http://localhost:8080/sum";
const SUBTRACT_URL: string = "http://localhost:8080/subtract";
const MULTIPLY_URL: string = "http://localhost:8080/multiply";
const DIVIDE_URL: string = "http://localhost:8080/divide";

const SUM_GET_URL: string = "http://localhost:8080/sum/3/9";

async function main() {
	// send a HTTP GET request to the API endpoint
	console.log(`Suma numerelor 2 si 3 este: ${await rp.post(SUM_URL, { json: true, body: { param1: "2", param2: "3" } })}`);
	console.log(`Diferenta numerelor 12 si 5 este: ${await rp.post(SUBTRACT_URL, { json: true, body: { param1: "12", param2: "5" } })}`);
	console.log(
		`Produsul numerelor 17.25 si 4 este: ${await rp.post(MULTIPLY_URL, { json: true, body: { param1: "17.25", param2: "4" } })}`
	);
	console.log(
		`Catul numerelor 15 si 0.172653 este: ${await rp.post(DIVIDE_URL, { json: true, body: { param1: "15", param2: "0.172653" } })}`
	);

	console.log(`Suma nuemrelor prin GET este: ${await rp.get(SUM_GET_URL)}`)

	// GET /userInformation/19407....
	// GET /login/mircea/parola
}

main();
