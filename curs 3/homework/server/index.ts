import express from "express";

// define the server port
const PORT: number = 8080;

// create the server
const app: express.Application = express();

// configure server
app.use(express.json());
app.use(express.urlencoded());

// define routes

app.post("/sum", (request: express.Request, response: express.Response) => {
	response.json(getResult(request.body.param1, request.body.param2, "+"));
});

app.post("/subtract", (request: express.Request, response: express.Response) => {
	response.json(getResult(request.body.param1, request.body.param2, "-"));
});

app.post("/multiply", (request: express.Request, response: express.Response) => {
	response.json(getResult(request.body.param1, request.body.param2, "*"));
});

app.post("/divide", (request: express.Request, response: express.Response) => {
	response.json(getResult(request.body.param1, request.body.param2, "/"));
});

app.get("/sum/:a/:b", (request: express.Request, response: express.Response) => {
	response.json(getResult(request.params.a, request.params.b, "+"));
});

// start the server on the specified port
app.listen(PORT, () => {
	console.log(`Server started at http://localhost:${PORT}!`);
});

function getResult(param1: string, param2: string, operator: string) {
	return eval(`${param1}${operator}${param2}`);
}
