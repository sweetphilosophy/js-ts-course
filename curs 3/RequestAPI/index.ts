import rp from 'request-promise';
import { Movie } from './Movie';

// define the API endpoint for the requests
const URL: string = 'https://ghibliapi.herokuapp.com/films';

async function main() {
	// send a HTTP GET request to the API endpoint
	let response = await rp.get(URL);

	// parse the JSON response and get an array of movie objects
	let movies = JSON.parse(response);

	// iterate through the movies
	movies.forEach((movie: Movie) => {
		// print movie title by accessing 'title' property
		// you can also access 'title' property like this: movie["title"]
		console.log(movie.title);
	});
}

main();
