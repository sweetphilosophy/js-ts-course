/**
 * The movie model for the data we receive from the API.
 * The movie objects have more properties than the title,
 * but for the sake of this example we defined just the 'title'.
 */
export class Movie {
	private _title: string;

	constructor(title: string) {
		this._title = title;
	}

	public get title(): string {
		return this._title;
	}
}
