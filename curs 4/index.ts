import rp from "request-promise";
// let count = 0;
// for(let i = 0; i < 1000000000; i++){
//     count += i;
// }

async function getMovie(name: string) {
	const URL: string = "https://ghibliapi.herokuapp.com/films";

	let result = await rp.get(URL, { json: true });
	let movie = result.find((movie: any) => movie.title === name);

	return movie;
}

async function main() {
	let movie = await getMovie("Castle in the Sky");
    console.log(movie);
}

main();

console.log("Salut")
