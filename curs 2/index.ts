// console.log("Hello world!");

// let nume = "Mircea";

// console.log("Hello " + nume);
// console.log('Hello "' + nume + '"');
// console.log(`Hello ${nume}`);

// function hello(name){
//     return "Hello " + name;
// }

// let hello = (name) => "Hello " + name;

// let testFunc = (x) => x + 1;

// let complexHello = (func, name, welcomePhrase) => func(name) + welcomePhrase;

// console.log(hello("Mircea"));

// console.log(complexHello(hello, "Mircea", " welcome to Cerner"));
// console.log(complexHello(testFunc, 123, 123));

// let person = { name: "Mircea", age: 26 };
// console.log("Hello " + person.name + " you are " + person.age + " years old.");

// let array = ["Mircea", 123, true, person];
// console.log(array);

// let age = 17;

// if (age >= 18) {
// 	console.log("Esti major");
// } else {
// 	console.log("esti minor");
// }

// let i = 0;
// while (i < 10) {
// 	console.log(i++);
// }

// for(let i = 0; i < 10; i++){
//     console.log(i)
// }

// let age = 20;

// let x = age >= 18 ? "Major" : "Minor";

// console.log(age >= 18 ? "Major" : "Minor")

// if age
//  x = ...
//  else
//  x = ...

// const PI = 3.14;

// PI = 2;

// alex.x;

// TYPESCRIPT
console.log("Hello typescript");

let x = "Mircea";

let sum = (a: number, b: number): number => a + b;

console.log(sum(2, 3));
